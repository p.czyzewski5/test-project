<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

define('VENDOR_PATH', realpath('../vendor'));

require_once VENDOR_PATH . '/autoload.php';

echo (new \recruitment\App())->run();

