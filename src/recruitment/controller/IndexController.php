<?php

namespace recruitment\controller;

use Exception;
use recruitment\process\exception\ValidationError;
use recruitment\process\index\Create;
use recruitment\process\index\DefaultData;
use recruitment\process\index\Index;
use recruitment\process\index\Truncate;
use recruitment\service\Ioc;

class IndexController extends AbstractController
{

    /**
     * @throws Exception
     */
    public function indexAction()
    {
        try {
            $process = Ioc::get(Index::class);
            $response = $process->handle($_GET);
        } catch (ValidationError $e) {
            var_dump('@todo obsługa wyjątków walidacji.');exit;
        }

        return $this->view->render('index', $response->getData());
    }

    /**
     * @throws Exception
     */
    public function defaultDataAction()
    {
        try {
            $process = Ioc::get(DefaultData::class);
            $process->handle($_GET);
        } catch (ValidationError $e) {
            var_dump('@todo obsługa wyjątków walidacji.');exit;
        }

        return header('Location: /');
    }

    /**
     * @throws Exception
     */
    public function truncateAction()
    {
        try {
            $process = Ioc::get(Truncate::class);
            $response = $process->handle($_GET);
        } catch (ValidationError $e) {
            var_dump('@todo obsługa wyjątków walidacji.');exit;
        }

        return $this->view->render('truncate', $response->getData());
    }

    /**
     * @throws Exception
     */
    public function createAction()
    {
        try {
            $process = Ioc::get(Create::class);
            $process->handle($_POST);
        } catch (ValidationError $e) {
            var_dump('@todo obsługa wyjątków walidacji.');exit;
        }

        return header('Location: /');
    }

}