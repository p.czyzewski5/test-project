<?php

namespace recruitment\controller;

use recruitment\component\Response;
use recruitment\component\ResponseInterface;
use recruitment\service\View;

abstract class AbstractController
{

    /** @var Response */
    protected $controllerResponse;

    /** @var View */
    protected $view;


    public function __construct(ResponseInterface $controllerResponse, View $view)
    {
        $this->controllerResponse = $controllerResponse;
        $this->view = $view;
    }


    protected function getResponse(): Response
    {
        return $this->controllerResponse;
    }

}