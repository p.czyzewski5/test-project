<?php

namespace recruitment\process\exception;

class ValidationError extends \DomainException
{

    protected $errorContainer = [];


    public function __construct(array $errorContainer, string $message = '', int $code = 0, \Throwable $previous = null)
    {
        $this->errorContainer = $errorContainer;

        parent::__construct($message, $code, $previous);
    }

    public function getErrorContainer(): array
    {
        return $this->errorContainer;
    }

}