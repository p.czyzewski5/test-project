<?php

namespace recruitment\process\index;

use recruitment\component\Response;
use recruitment\process\AbstractProcess;
use recruitment\repository\Book;
use recruitment\repository\Magazine;
use recruitment\repository\Poster;

class Truncate extends AbstractProcess
{

    /** @var Book  */
    private $bookRepository;

    /** @var Magazine */
    private $magazineRepository;

    /** @var Poster */
    private $posterRepository;


    public function __construct(
        Response $response,
        Book $bookRepository,
        Magazine $magazineRepository,
        Poster $posterRepository
    ) {
        $this->bookRepository = $bookRepository;
        $this->magazineRepository = $magazineRepository;
        $this->posterRepository = $posterRepository;

        parent::__construct($response);
    }


    protected function validate(array $options = [])
    {
    }

    protected function sanitize(array $options = []): array
    {
        return $options;
    }

    protected function execute(array $options = [])
    {
        try {
            $count = $this->bookRepository->count();
            $this->bookRepository->truncate();

            $count += $this->magazineRepository->count();
            $this->magazineRepository->truncate();


            $count += $this->posterRepository->count();
            $this->posterRepository->truncate();

            $this->getResponse()->setData([
                'deletedCount' => (int)$count,
            ]);
        } catch (\Exception $e) {
            $this->getResponse()->addError($e->getMessage());
        }
    }

}