<?php

namespace recruitment\process\index;

use recruitment\component\Response;
use recruitment\process\AbstractProcess;
use recruitment\repository\Book;
use recruitment\repository\Magazine;
use recruitment\repository\Poster;

class Create extends AbstractProcess
{

    /** @var Book  */
    private $bookRepository;

    /** @var Magazine */
    private $magazineRepository;

    /** @var Poster */
    private $posterRepository;


    public function __construct(
        Response $response,
        Book $bookRepository,
        Magazine $magazineRepository,
        Poster $posterRepository
    ) {
        $this->bookRepository = $bookRepository;
        $this->magazineRepository = $magazineRepository;
        $this->posterRepository = $posterRepository;

        parent::__construct($response);
    }


    protected function validate(array $options = [])
    {
        // @todo walidacja
    }

    protected function sanitize(array $options = []): array
    {
        // @todo sanitize

        return $options;
    }

    protected function execute(array $options = [])
    {
        try {
            var_dump($options);exit;
        } catch (\Exception $e) {
            $this->getResponse()->addError($e->getMessage());
        }
    }

}