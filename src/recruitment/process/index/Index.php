<?php

namespace recruitment\process\index;

use recruitment\component\Response;
use recruitment\process\AbstractProcess;
use recruitment\repository\Repository;

class Index extends AbstractProcess
{

    /** @var Repository  */
    private $repository;


    public function __construct(Response $response, Repository $bookRepository)
    {
        $this->repository = $bookRepository;

        parent::__construct($response);
    }


    protected function validate(array $options = [])
    {
    }

    protected function sanitize(array $options = []): array
    {
        return $options;
    }

    protected function execute(array $options = [])
    {
        try {
            $this->getResponse()->setData([
                'publicationSummary' => $this->repository->getPublications() ?? [],
                'statistics' => $this->repository->getStatistics() ?? []
            ]);
        } catch (\Exception $e) {
            $this->getResponse()->addError($e->getMessage());
        }
    }

}