<?php

namespace recruitment\process;

use recruitment\component\ResponseInterface;

abstract class AbstractProcess
{

    /** @var ResponseInterface */
    protected $processResponse;


    public function __construct(ResponseInterface $processResponse)
    {
        $this->processResponse = $processResponse;
    }

    final public function handle(array $options = []): ResponseInterface
    {
        $this->validate($options);

        $options = $this->sanitize($options);

        if ($this->getResponse()->getStatus() === false) {
            throw new \recruitment\process\exception\ValidationError($this->getResponse()->getErrors());
        }

        $this->execute($options);

        return $this->getResponse();
    }


    protected function getResponse(): ResponseInterface
    {
        return $this->processResponse;
    }


    abstract protected function validate(array $options = []);

    abstract protected function sanitize(array $options = []): array;

    abstract protected function execute(array $options = []);

}