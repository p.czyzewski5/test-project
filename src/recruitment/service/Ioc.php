<?php

namespace recruitment\service;

class Ioc
{

    private static $containers = [];

    private static $sharedContainers = [];


    public static function register(string $name, callable $callable)
    {
        self::$containers[$name] = $callable;
    }

    /**
     * @throws \Exception
     */
    public static function get(string $name)
    {
        if (array_key_exists($name, self::$containers) === false) {
            throw new \Exception("Container: {$name}, not found!");
        }

        return (self::$containers[$name])();
    }

    /**
     * @throws \Exception
     */
    public static function getShared(string $name)
    {
        if (array_key_exists($name, self::$sharedContainers) === false) {
            self::$sharedContainers[$name] = self::get($name);
        }

        return self::$sharedContainers[$name];
    }

}