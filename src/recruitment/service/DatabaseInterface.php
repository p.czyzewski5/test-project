<?php

namespace recruitment\service;

interface DatabaseInterface
{

    public function __construct(array $config);

    public function getConnection();

}