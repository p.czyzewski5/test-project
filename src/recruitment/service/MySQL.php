<?php

namespace recruitment\service;

class MySQL implements DatabaseInterface
{

    const REQUIRED_DB_CONFIG_FIELDS = ['host', 'db_name', 'username', 'password'];


    /**
     * @var array
     */
    private $databaseConfig;

    /**
     * @var \PDO
     */
    private $connection;


    public function __construct(array $databaseConfig)
    {
        foreach (self::REQUIRED_DB_CONFIG_FIELDS as $field) {
            if (isset($databaseConfig[$field]) === false
                || empty($databaseConfig[$field])
            ) {
                throw new \PDOException("Database configuration error, field: '{$field}', was not set!");
            }
        }

        $this->databaseConfig = $databaseConfig;
    }

    public function getConnection(): \PDO
    {
        if (empty($this->connection) === false) {
            return $this->connection;
        }

        $this->connection = new \PDO(
            "mysql:host={$this->databaseConfig['host']};dbname={$this->databaseConfig['db_name']}",
            $this->databaseConfig['username'],
            $this->databaseConfig['password']
        );

        return $this->connection;
    }

}