<?php

namespace recruitment\service;

class View
{

    /**
     * @throws \Exception
     */
    public function render($view, $params = null)
    {
        $controller = Ioc::getShared(Router::class)->getMatchedRoute()['controller'];
        $controller = (new \ReflectionClass($controller))->getShortName();
        $controllerDir = str_replace('Controller', '', $controller);
        $controllerDir = strtolower($controllerDir);

        $viewPath = \recruitment\App::VIEWS_PATH . "/{$controllerDir}/{$view}.php";

        if (file_exists($viewPath) === false) {
            throw new \Exception("View not found: {$viewPath}");
        }

        ob_start();
        extract($params);
        include_once($viewPath);
        return ob_get_clean();
    }

}