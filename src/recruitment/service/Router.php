<?php

namespace recruitment\service;

class Router
{

    private $routes = [];

    /** @var string */
    private $matchedRoute;


    /**
     * @throws \Exception
     */
    public function __construct(array $routes)
    {
        foreach ($routes as $route) {
            if (isset($route['controller']) === false
                || isset($route['action']) === false
            ) {
                throw new \Exception('Invalid routes configuration, controller or action was not set!');
            }
        }

        $this->routes = $routes;
    }

    /**
     * @throws \Exception
     */
    public function handle()
    {
        $this->matchRoute(
            filter_var($_SERVER['REQUEST_URI'], FILTER_SANITIZE_URL)
        );

        if (empty($this->matchedRoute) || class_exists($this->matchedRoute['controller']) === false) {
            return header('http/1.0 404 not found');
        }

        $controller = Ioc::get($this->getMatchedRoute()['controller']);
        $action = "{$this->getMatchedRoute()['action']}Action";

        return $controller->$action();
    }

    public function getMatchedRoute()
    {
        return $this->matchedRoute;
    }


    private function matchRoute(string $requestUri)
    {
        if (in_array($requestUri, array_keys($this->routes))) {
            $this->matchedRoute = $this->routes[$requestUri];
        }
    }

}