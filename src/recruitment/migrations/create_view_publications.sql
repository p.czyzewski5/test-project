CREATE VIEW publications AS
  SELECT
    book_id as id, title, pages_count as 'pages count / dimensions', copies_count as 'copies count', publication_year as 'publication year',
    CONCAT('original languague ISO code: ', original_language_ISO_code, ', ',
           'copy languague ISO code: ', copy_language_ISO_code) as 'additional info'
  FROM book
  UNION ALL
  SELECT
    magazine_id, title, pages_count, copies_count, publication_year, concat('issue: ', issue)
  FROM magazine
  UNION ALL
  SELECT
    poster_id, title, concat_ws(' x ', width, height), copies_count, publication_year, 'n/a'
  FROM poster;