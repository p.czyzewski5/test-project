CREATE TABLE `magazine` (
  `magazine_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `issue` int(10) unsigned NOT NULL,
  `pages_count` int(10) unsigned NOT NULL,
  `copies_count` int(10) unsigned NOT NULL,
  `publication_year` year(4) NOT NULL,
  PRIMARY KEY (`magazine_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci