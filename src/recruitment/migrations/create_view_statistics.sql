CREATE VIEW statistics AS
SELECT unique_titles, (book_count+magazine_count+poster_count) as total, book_count, magazine_count, poster_count
FROM
  (
    SELECT
      COUNT(*) as book_count
    FROM book
  ) as b,
  (
    SELECT
      COUNT(*) as magazine_count
    FROM magazine
  ) as m,
  (
    SELECT
      COUNT(*) as poster_count
    FROM poster
  ) as p,
  (
    SELECT DISTINCT count(title) as unique_titles FROM
      (
        SELECT title FROM
          book
        UNION
        SELECT DISTINCT title FROM
          magazine
        UNION
        SELECT DISTINCT title FROM
          poster
      ) as u ) as d