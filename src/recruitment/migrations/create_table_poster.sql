CREATE TABLE `poster` (
  `poster_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `width` int(10) unsigned NOT NULL,
  `height` int(10) unsigned NOT NULL,
  `copies_count` int(10) unsigned NOT NULL,
  `publication_year` year(4) NOT NULL,
  PRIMARY KEY (`poster_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci