CREATE TABLE `book` (
  `book_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pages_count` int(10) unsigned NOT NULL,
  `copies_count` int(10) unsigned NOT NULL,
  `publication_year` year(4) NOT NULL,
  `original_language_ISO_code` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `copy_language_ISO_code` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`book_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci