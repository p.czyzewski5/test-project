<?php

namespace recruitment\component;

class Response implements ResponseInterface, ErrorContainerInterface
{

    use ErrorContainerTrait;


    protected $data = [];


    public function getStatus(): bool
    {
        return $this->hasErrors() === false;
    }

    public function getData(): array
    {
        return $this->data;
    }

    public function setData(array $data): Response
    {
        $this->data = $data;

        return $this;
    }

}