<?php

namespace recruitment\component;

interface ResponseInterface
{

    public function getStatus(): bool;

    public function getData(): array;

}