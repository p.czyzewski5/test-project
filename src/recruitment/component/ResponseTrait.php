<?php

namespace recruitment\component;

trait ResponseTrait
{

    protected $response;


    public function getResponse(): Response
    {
        return $this->response;
    }

    public function setResponse(Response $response): self
    {
        $this->response = $response;

        return $this;
    }

}