<?php

namespace recruitment\component;

trait ErrorContainerTrait
{

    protected $errorContainer = [];


    public function addError(string $error): ErrorContainerInterface
    {
        $this->errorContainer[] = $error;

        return $this;
    }

    public function getErrors(): array
    {
        return $this->errorContainer;
    }

    public function hasErrors(): bool
    {
        return empty($this->errorContainer) === false;
    }

    public function clearErrors(): ErrorContainerInterface
    {
        $this->errorContainer = [];

        return $this;
    }

}