<?php

namespace recruitment\component;

interface ErrorContainerInterface {

    public function addError(string $error): self;

    public function getErrors(): array;

    public function hasErrors(): bool;

    public function clearErrors(): self;

}

