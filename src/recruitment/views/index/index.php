<a class="button safe" href="/default-data">Wgraj losowe dane</a>
<a class="button danger" href="/truncate">Opróżnij tabele</a>

<h2>Lista elementów</h2>
<table>
    <?php if (empty($publicationSummary) === false): ?>
        <tr>
            <th>no.</th>
            <th><?= implode('</th><th>', array_keys($publicationSummary[0])) ?></th>
        </tr>
    <?php endif; ?>
    <?php $no = 1; ?>
    <?php foreach ($publicationSummary as $publication): ?>
        <tr>
            <td><?= $no ?></td>
            <td><?= implode('</td><td>', $publication) ?></td>
        </tr>
        <?php $no++; ?>
    <?php endforeach; ?>
</table>

<h2>Statystyki</h2>
<table>
    <?php if (empty($statistics) === false): ?>
        <tr>
            <th>no.</th>
            <th><?= implode('</th><th>', array_keys($statistics[0])) ?></th>
        </tr>
    <?php endif; ?>
    <?php $no = 1; ?>
    <?php foreach ($statistics as $statistic): ?>
        <tr>
            <td><?= $no ?></td>
            <td><?= implode('</td><td>', $statistic) ?></td>
        </tr>
        <?php $no++; ?>
    <?php endforeach; ?>
</table>

<h2>Formularz</h2>
<form action="/create" method="post">
    <input name="test">
    <input type="submit">
</form>

<style>
    .button {
        color: white;
        padding: 8px 12px;
        border-radius: 8px;
        text-decoration: none;
        border: solid 1px;
    }
    .button.safe {
        border-color: green;
        background-color: forestgreen;
    }
    .button.danger {
        border-color: indianred;
        background-color: darkred;
    }

    table {
        border-collapse: collapse;
    }
    table tr td, th {
        border: 1px solid black;
        padding: 3px 4px;
    }
</style>