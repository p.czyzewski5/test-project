<?php

namespace recruitment\model\magazine;

use recruitment\component\ErrorContainerInterface;
use recruitment\component\ErrorContainerTrait;
use recruitment\model\AbstractModel;
use recruitment\model\ModelInterface;

class Magazine extends AbstractModel implements ErrorContainerInterface, ModelInterface
{

    use ErrorContainerTrait;


    /** @var int */
    private $magazine_id;

    /** @var string */
    private $title;

    /** @var int */
    private $issue;

    /** @var int */
    private $pages_count;

    /** @var int */
    private $copies_count;

    /** @var int */
    private $publication_year;


    public function setMagazineId(int $magazineId): Magazine
    {
        $this->magazine_id = $magazineId;
        return $this;
    }

    public function setTitle(string $title): Magazine
    {
        $this->title = $title;
        return $this;
    }

    public function setIssue(int $issue): Magazine
    {
        $this->issue = $issue;
        return $this;
    }

    public function setPagesCount(int $pagesCount): Magazine
    {
        $this->pages_count = $pagesCount;
        return $this;
    }

    public function setCopiesCount(int $copiesCount): Magazine
    {
        $this->copies_count = $copiesCount;
        return $this;
    }

    public function setPublicationYear(int $publicationYear): Magazine
    {
        $this->publication_year = $publicationYear;
        return $this;
    }

    public function getMagazineId(): ?int
    {
        return $this->magazine_id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function getIssue(): ?int
    {
        return $this->issue;
    }

    public function getPagesCount(): ?int
    {
        return $this->pages_count;
    }

    public function getCopiesCount(): ?int
    {
        return $this->copies_count;
    }

    public function getPublicationYear(): ?int
    {
        return $this->publication_year;
    }

    public static function getSource(): string
    {
        return 'magazine';
    }

}