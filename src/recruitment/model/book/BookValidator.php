<?php

namespace recruitment\model\book;

use recruitment\component\ErrorContainerInterface;

class BookValidator
{

    public static function validate(array $data, ErrorContainerInterface $errorContainer): void
    {
        if (is_null($data['title']) || strlen($data['title']) === 0) {
            $errorContainer->addError('Value of title is required!');
        }
    }

}
