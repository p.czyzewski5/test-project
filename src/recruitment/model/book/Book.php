<?php

namespace recruitment\model\book;

use recruitment\component\ErrorContainerInterface;
use recruitment\component\ErrorContainerTrait;
use recruitment\model\AbstractModel;
use recruitment\model\ModelInterface;

class Book extends AbstractModel implements ErrorContainerInterface, ModelInterface
{

    use ErrorContainerTrait;


    /** @var int */
    private $book_id;

    /** @var string */
    private $title;

    /** @var int */
    private $pages_count;

    /** @var int */
    private $copies_count;

    /** @var int */
    private $publication_year;

    /** @var string */
    private $original_language_ISO_code;

    /** @var string */
    private $copy_language_ISO_code;


    public function setBookId(int $bookId): Book
    {
        $this->book_id = $bookId;
        return $this;
    }

    public function setTitle(string $title): Book
    {
        $this->title = $title;
        return $this;
    }

    public function setPagesCount(int $pagesCount): Book
    {
        $this->pages_count = $pagesCount;
        return $this;
    }

    public function setCopiesCount(int $copiesCount): Book
    {
        $this->copies_count = $copiesCount;
        return $this;
    }

    public function setPublicationYear(int $publicationYear): Book
    {
        $this->publication_year = $publicationYear;
        return $this;
    }

    public function setOriginalLanguageISOCode(string $originalLanguageISOCode): Book
    {
        $this->original_language_ISO_code = $originalLanguageISOCode;
        return $this;
    }

    public function setCopyLanguageISOCode(string $copyLanguageISOCode): Book
    {
        $this->copy_language_ISO_code = $copyLanguageISOCode;
        return $this;
    }

    public function getBookId(): ?int
    {
        return $this->book_id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function getPagesCount(): ?int
    {
        return $this->pages_count;
    }

    public function getCopiesCount(): ?int
    {
        return $this->copies_count;
    }

    public function getPublicationYear(): ?int
    {
        return $this->publication_year;
    }

    public function getOriginalLanguageISOCode(): ?string
    {
        return $this->original_language_ISO_code;
    }

    public function getCopyLanguageISOCode(): ?string
    {
        return $this->copy_language_ISO_code;
    }

    public static function getSource(): string
    {
        return 'book';
    }

}