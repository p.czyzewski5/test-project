<?php

namespace recruitment\model;

interface ModelInterface
{

    public static function getSource(): string;

    public function toArray(): array;

}