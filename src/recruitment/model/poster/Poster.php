<?php

namespace recruitment\model\poster;

use recruitment\component\ErrorContainerInterface;
use recruitment\component\ErrorContainerTrait;
use recruitment\model\AbstractModel;
use recruitment\model\ModelInterface;

class Poster extends AbstractModel implements ErrorContainerInterface, ModelInterface
{

    use ErrorContainerTrait;


    /** @var int */
    private $poster_id;

    /** @var string */
    private $title;

    /** @var int */
    private $width;

    /** @var int */
    private $height;

    /** @var int */
    private $copies_count;

    /** @var int */
    private $publication_year;


    public function setPosterId(int $posterId): Poster
    {
        $this->poster_id = $posterId;
        return $this;
    }

    public function setTitle(string $title): Poster
    {
        $this->title = $title;
        return $this;
    }

    public function setWidth(int $width): Poster
    {
        $this->width = $width;
        return $this;
    }

    public function setHeight(int $height): Poster
    {
        $this->height = $height;
        return $this;
    }

    public function setCopiesCount(int $copiesCount): Poster
    {
        $this->copies_count = $copiesCount;
        return $this;
    }

    public function setPublicationYear(int $publicationYear): Poster
    {
        $this->publication_year = $publicationYear;
        return $this;
    }

    public function getPosterId(): ?int
    {
        return $this->poster_id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function getWidth(): ?int
    {
        return $this->width;
    }

    public function getHeight(): ?int
    {
        return $this->height;
    }

    public function getCopiesCount(): ?int
    {
        return $this->copies_count;
    }

    public function getPublicationYear(): ?int
    {
        return $this->publication_year;
    }

    public static function getSource(): string
    {
        return 'poster';
    }

}