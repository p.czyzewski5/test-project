<?php

namespace recruitment\repository;

use recruitment\model\ModelInterface;
use recruitment\service\DatabaseInterface;
use recruitment\service\MySQL;

class AbstractRepository
{

    /** @var MySQL */
    public $database;

    /** @var ModelInterface */
    public $model;


    public function __construct(DatabaseInterface $database, ModelInterface $model = null)
    {
        $this->database = $database;
        $this->model = $model;
    }

    public function count(): ?int
    {
        $statement = $this->database->getConnection()->prepare(
            'SELECT COUNT(*) FROM ' . $this->model::getSource()
        );

        $result = $statement->execute();

        if ($result === false) {
            throw new \PDOException(print_r($statement->errorInfo()));
        }

        return $statement->fetch(\PDO::FETCH_COLUMN) ?: null;
    }

    public function truncate()
    {
        $statement = $this->database->getConnection()->prepare(
            'TRUNCATE TABLE ' . $this->model::getSource()
        );

        $result = $statement->execute();

        if ($result === false) {
            throw new \PDOException(print_r($statement->errorInfo()));
        }
    }

}