<?php

namespace recruitment\repository;

class Repository extends AbstractRepository
{

    public function getPublications()
    {
        $statement = $this->database->getConnection()->prepare(
            'SELECT * FROM publications'
        );

        $result = $statement->execute();

        if ($result === false) {
            throw new \PDOException(print_r($statement->errorInfo()));
        }

        return $statement->fetchAll(\PDO::FETCH_ASSOC) ?: null;
   }

    public function getStatistics()
    {
        $statement = $this->database->getConnection()->prepare(
            'SELECT * FROM statistics'
        );

        $result = $statement->execute();

        if ($result === false) {
            throw new \PDOException(print_r($statement->errorInfo()));
        }

        return $statement->fetchAll(\PDO::FETCH_ASSOC) ?: null;
    }

}