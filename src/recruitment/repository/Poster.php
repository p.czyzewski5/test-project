<?php

namespace recruitment\repository;

class Poster extends AbstractRepository
{

    public function createTestData()
    {
        $faker = \Faker\Factory::create();

        for ($ii = 0; $ii < 10; $ii++) {

            $data = [
                'title' => $faker->country,
                'width' => $faker->randomDigitNotNull,
                'height' => $faker->randomDigitNotNull,
                'copies_count' => $faker->randomDigitNotNull,
                'publication_year' => $faker->year,
            ];

            $names = implode(', ', array_keys($data));
            $placeholders = array_map(function(string $key) {
                return ":{$key}";
            }, array_keys($data));
            $placeholders = implode(', ', $placeholders);

            $statement = $this->database->getConnection()->prepare(
                "INSERT INTO poster ({$names}) VALUES ({$placeholders})"
            );

            $result = $statement->execute($data);

            if ($result === false) {
                throw new \PDOException(print_r($statement->errorInfo()));
            }
        }
    }

}