<?php

return [
    '/' => [
        'controller' => \recruitment\controller\IndexController::class,
        'action' => 'index'
    ],
    '/truncate' => [
        'controller' => \recruitment\controller\IndexController::class,
        'action' => 'truncate'
    ],
    '/default-data' => [
        'controller' => \recruitment\controller\IndexController::class,
        'action' => 'defaultData'
    ],
    '/create' => [
        'controller' => \recruitment\controller\IndexController::class,
        'action' => 'create'
    ],
];