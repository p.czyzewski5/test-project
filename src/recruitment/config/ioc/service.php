<?php

\recruitment\service\Ioc::register(\recruitment\service\MySQL::class, function() {
    $config = include_once(\recruitment\App::CONFIG_PATH . '/config.php');
    $service = new \recruitment\service\MySQL($config['database']);

    return $service;
});

\recruitment\service\Ioc::register(\recruitment\service\Router::class, function() {
    $config = include_once(\recruitment\App::CONFIG_PATH . '/routes.php');
    $service = new \recruitment\service\Router($config);

    return $service;
});