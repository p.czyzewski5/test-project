<?php

\recruitment\service\Ioc::register(\recruitment\controller\IndexController::class, function() {
    $response = new \recruitment\component\Response();
    $view = new \recruitment\service\View();
    $controller = new \recruitment\controller\IndexController($response, $view);

    return $controller;
});