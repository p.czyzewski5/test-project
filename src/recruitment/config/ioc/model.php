<?php

\recruitment\service\Ioc::register(\recruitment\model\book\Book::class, function() {
    $model = new \recruitment\model\book\Book();

    return $model;
});

\recruitment\service\Ioc::register(\recruitment\model\magazine\Magazine::class, function() {
    $model = new \recruitment\model\magazine\Magazine();

    return $model;
});

\recruitment\service\Ioc::register(\recruitment\model\poster\Poster::class, function() {
    $model = new \recruitment\model\poster\Poster();

    return $model;
});