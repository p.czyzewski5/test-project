<?php

\recruitment\service\Ioc::register(\recruitment\process\index\Index::class, function() {
    $response = new \recruitment\component\Response();
    $repository = \recruitment\service\Ioc::get(\recruitment\repository\Repository::class);
    $process = new \recruitment\process\index\Index($response, $repository);

    return $process;
});

\recruitment\service\Ioc::register(\recruitment\process\index\DefaultData::class, function() {
    $response = new \recruitment\component\Response();
    $bookRepository = \recruitment\service\Ioc::get(\recruitment\repository\Book::class);
    $magazineRepository = \recruitment\service\Ioc::get(\recruitment\repository\Magazine::class);
    $posterRepository = \recruitment\service\Ioc::get(\recruitment\repository\Poster::class);
    $process = new \recruitment\process\index\DefaultData(
        $response,
        $bookRepository,
        $magazineRepository,
        $posterRepository
    );

    return $process;
});

\recruitment\service\Ioc::register(\recruitment\process\index\Truncate::class, function() {
    $response = new \recruitment\component\Response();
    $bookRepository = \recruitment\service\Ioc::get(\recruitment\repository\Book::class);
    $magazineRepository = \recruitment\service\Ioc::get(\recruitment\repository\Magazine::class);
    $posterRepository = \recruitment\service\Ioc::get(\recruitment\repository\Poster::class);
    $process = new \recruitment\process\index\Truncate(
        $response,
        $bookRepository,
        $magazineRepository,
        $posterRepository
    );

    return $process;
});

\recruitment\service\Ioc::register(\recruitment\process\index\Create::class, function() {
    $response = new \recruitment\component\Response();
    $bookRepository = \recruitment\service\Ioc::get(\recruitment\repository\Book::class);
    $magazineRepository = \recruitment\service\Ioc::get(\recruitment\repository\Magazine::class);
    $posterRepository = \recruitment\service\Ioc::get(\recruitment\repository\Poster::class);
    $process = new \recruitment\process\index\Create(
        $response,
        $bookRepository,
        $magazineRepository,
        $posterRepository
    );

    return $process;
});
