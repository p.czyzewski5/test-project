<?php

\recruitment\service\Ioc::register(\recruitment\repository\Book::class, function() {
    $database = \recruitment\service\Ioc::getShared(\recruitment\service\MySQL::class);
    $model = \recruitment\service\Ioc::get(\recruitment\model\book\Book::class);
    $repository = new \recruitment\repository\Book($database, $model);

    return $repository;
});

\recruitment\service\Ioc::register(\recruitment\repository\Magazine::class, function() {
    $database = \recruitment\service\Ioc::getShared(\recruitment\service\MySQL::class);
    $model = \recruitment\service\Ioc::get(\recruitment\model\magazine\Magazine::class);
    $repository = new \recruitment\repository\Magazine($database, $model);

    return $repository;
});

\recruitment\service\Ioc::register(\recruitment\repository\Poster::class, function() {
    $database = \recruitment\service\Ioc::getShared(\recruitment\service\MySQL::class);
    $model = \recruitment\service\Ioc::get(\recruitment\model\poster\Poster::class);
    $repository = new \recruitment\repository\Poster($database, $model);

    return $repository;
});

\recruitment\service\Ioc::register(\recruitment\repository\Repository::class, function() {
    $database = \recruitment\service\Ioc::getShared(\recruitment\service\MySQL::class);
    $repository = new \recruitment\repository\Repository($database);

    return $repository;
});