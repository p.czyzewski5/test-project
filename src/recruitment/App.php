<?php

namespace recruitment;

use recruitment\service\Ioc;
use recruitment\service\Router;

class app
{

    const
        CONFIG_PATH = __DIR__ . '/config',
        VIEWS_PATH = __DIR__ . '/views';


    public function __construct()
    {
        $this->registerIoc();
    }

    /**
     * @throws \Exception
     */
    public function run()
    {
        $router = Ioc::getShared(Router::class);

        return $router->handle();
    }


    private function registerIoc()
    {
        if (file_exists($path = __DIR__ . '/config/ioc') === false) {
            return;
        }

        foreach (glob("{$path}/*.php") as $filename) {
            include_once($filename);
        }
    }

}